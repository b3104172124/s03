package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;

public interface PostService {

    void createPost(String stringToken, Post post );
    // Now that we are generating JWT, ownership of a blogpost will be retrieved
    Iterable<Post> getPosts();
}
